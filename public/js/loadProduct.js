
var productos
var inputValue = ""
var filtroMarca = "*"
var systemCategory = "EMBRAGUE_Y_CARDAN"
var marcaSearch = "*"
var productoSearch = "*"

function loadProductos() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            optionMarcas()
        }
    });
}

function loadEmbrague() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("EMBRAGUE_Y_CARDAN", 1)
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });


}

function loadEncendido() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("ENCENDIDO", 1)
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function loadFrenos() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("FRENOS", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });

}

function loadGasolina() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("GASOLINA", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function loadMiscelaneos() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("MISCELANEOS", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function loadMotor() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("MOTOR", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function loadEnfriamiento() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("ENFRIAMIENTO", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function loadQuimicos() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("QUIMICOS", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function loadSuspension() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("SUSPENSION_Y_DIRECCION", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function loadTransmision() {
    $.ajax({
        'url': "json/productos.json",
        'dataType': "json",
        'success': function (data) {
            productos = data;
            addProducts("TRANSMISION_Y_EJES", 1);
            if (sessionStorage.producto || sessionStorage.marca) {
                filterSearch(sessionStorage.system, sessionStorage.producto, sessionStorage.marca)
            }
        }
    });
}

function addProducts(category, page) {
    var containerImages = document.getElementsByClassName("product-listing page-section-ptb")[0].children[0].children[0].children[1].children[1];
    var html = ""
    html = contructorHTML(category, page)
    containerImages.innerHTML = html
}

function contructorHTML(category, page) {
    let html = ""
    var initialPage = 12 * (page - 1);
    var finalPage = page * 12;
    var countImages = 0;
    var containerSelect = document.getElementById("selectorMarca");
    var options = "<option value='*' > TODOS </option>";
    for (var marca in productos[category]) {
        options += "<option value='" + marca + "'>" + marca + "</option>"
        for (var producto in productos[category][marca]) {
            countImages++;
            if (countImages > initialPage && countImages <= finalPage) {
                html = html + "<div class='col-lg-4'>" +
                    " <div class='car-item gray-bg text-center'>" +
                    "<div class='car-image'>" +
                    "<img class='img-fluid' src='img/" + productos[category][marca][producto] + " ' alt=''>" +
                    "<div class='car-overlay-banner'>" +
                    " </div>" +
                    "</div>" +
                    "<div class='car-content'>" +
                    "<a href=''>" + marca + "</a>" +
                    "<div class='separator'></div>" +
                    "<div class='price'>" +
                    "<span class='new-price'>" + producto + "</span>" +
                    " </div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
            }
        }

    }
    if (countImages >= 12)
        addPagination(page, countImages, category)
    containerSelect.innerHTML = options
    return html
}

function addPagination(page, countImages, section) {
    var totalPages = Math.trunc(countImages / 12) + ((countImages % 12) > 0 ? 1 : 0)
    var barPage = document.getElementsByClassName("pagination")[0]
    var html = ""
    for (var i = 1; i <= totalPages; i++) {
        html = html + "<div class='item'><li" + (i === page ? " class='active' >" : ">") +
            "<a onclick=\"return addProductsSearch('" + section + "'," + i +
            ")\">" + i +
            "</a></li></div>";
    }
    barPage.innerHTML = html

}

function searchPiece(category, piece, ele) {
    inputValue = piece !== "" ? piece : ele.value;
    addProductsSearch(category, 1)
}

function addProductsSearch(category, page) {
    var containerImages = document.getElementsByClassName("product-listing page-section-ptb")[0].children[0].children[0].children[1].children[1];
    var html = ""
    html = contructorHTMLSearch(category, page)
    containerImages.innerHTML = html
}

function contructorHTMLSearch(category, page) {
    let html = ""
    var initialPage = 12 * (page - 1);
    var finalPage = page * 12;
    var countImages = 0;
    for (var marca in productos[category]) {
        for (var producto in productos[category][marca]) {
            if (producto.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0 && ((filtroMarca.toLowerCase() === '*') || (marca.toLowerCase() === filtroMarca.toLowerCase()))) {
                countImages++;
                if (countImages > initialPage && countImages <= finalPage) {
                    html = html + "<div class='col-lg-4'>" +
                        " <div class='car-item gray-bg text-center'>" +
                        "<div class='car-image'>" +
                        "<img class='img-fluid' src='img/" + productos[category][marca][producto] + " ' alt=''>" +
                        "<div class='car-overlay-banner'>" +
                        " </div>" +
                        "</div>" +
                        "<div class='car-content'>" +
                        "<a href=''>" + marca + "</a>" +
                        "<div class='separator'></div>" +
                        "<div class='price'>" +
                        "<span class='new-price'>" + producto + "</span>" +
                        " </div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
                }
            }
        }

    }
    addPagination(page, countImages, category)
    return html
}

function selectMarca(category, marcaSearch) {
    filtroMarca = marcaSearch ? marcaSearch : event.target.value
    addProductsSearch(category, 1)
}

/*search main page*/

function optionMarcas() {
    var containerSelect = document.getElementById("selectorMarca");
    var options = "<option value='*' > TODOS </option>";
    for (var marca in productos[systemCategory]) {
        options += "<option value='" + marca + "'>" + marca + "</option>"
    }
    containerSelect.innerHTML = options
    optionProducts("")
}

function optionProducts(marca) {
    var containerSelect = document.getElementById("selectorProducto");
    var options = "<option value='*' > TODOS </option>";
    if (marca === "") {
        for (var marcaArray in productos[systemCategory]) {
            for (var producto in productos[systemCategory][marcaArray]) {
                options += "<option value='" + producto + "'>" + producto + "</option>"
            }
        }
    } else {
        for (var producto in productos[systemCategory][marca]) {
            options += "<option value='" + producto + "'>" + producto + "</option>"
        }
    }
    containerSelect.innerHTML = options
}

function selectSystem(sistema) {
    systemCategory = sistema;
    marcaSearch = "*";
    productoSearch = "*";
    optionMarcas()
}

function selectMarcaSearch(marca) {
    marcaSearch = marca
    productoSearch = "*";
    optionProducts(marca)
}

function selectProducto(producto) {
    productoSearch = producto
}

function searchRefaccion() {
    sessionStorage.setItem('system', systemCategory)
    sessionStorage.setItem('marca', marcaSearch)
    sessionStorage.setItem('producto', productoSearch)
    switch (systemCategory) {
        case "EMBRAGUE_Y_CARDAN":
            location.href = "./productos_embrague_cardan.html";
            break;
        case "ENCENDIDO":
            location.href = "./productos_encendido.html";
            break;
        case "FRENOS":
            location.href = "./refacciones-automotrices-por-mayoreo-Guadalajara-Monterrey.html";
            break;
        case "GASOLINA":
            location.href = "./productos_gasolina.html";
            break;
        case "MISCELANEOS":
            location.href = "./productos_miscelaneos.html";
            break;
        case "MOTOR":
            location.href = "./productos_motor.html";
            break;
        case "ENFRIAMIENTO":
            location.href = "./productos_enfriamiento.html";
            break;
        case "QUIMICOS":
            location.href = "./productos_quimicos.html";
            break;
        case "SUSPENSION_Y_DIRECCION":
            location.href = "./productos_suspension_direccion.html";
            break;
        case "TRANSMISION_Y_EJES":
            location.href = "./productos_transmision_ejes.html";
            break;
    }
}

function filterSearch(sistema, producto, marca) {
    var sel = document.getElementById('selectorMarca');
    var opts = sel.options;
    for (var opt, j = 0; opt = opts[j]; j++) {
        if (opt.value == marca) {
            sel.selectedIndex = j;
            break;
        }
    }
    selectMarca(sistema, marca)

    var input = document.getElementById('inputSearchPiece')
    let valueP = producto !== '*' ? producto : ""
    input.value = valueP
    searchPiece(sistema, valueP, this)
    sessionStorage.clear()

}
